<h1 align="center">
	<br>
	<img width="1252" src="resourses/logo.jpg" alt="resume">
	<br>
	<br>
	<br>
</h1>

# The scope of libraries that I use for professional development and testing

## Contents

- [Platforms](#platforms)
- [Programming Languages](#programming-languages)
- [Databases](#databases)
- [Learn](#learn)
- [Books](#books)

## Platforms
- [iOS](#ios)
- [Android](#android)

## Programming Languages

- [C/C++](#c/c++)
- [Java](#java)
- [Rx](#rx)
- [Kotlin](#kotlin)

## Databases
- [Sqlite](https://www.sqlite.org/)
- [Realm](https://realm.io/)
 
## IOS

- [Carthage](https://github.com/Carthage/Carthage)
- [RxAlamofire](https://github.com/RxSwiftCommunity/RxAlamofire)
- [Async](https://github.com/duemunk/Async)
- [Kingfisher](https://github.com/onevcat/Kingfisher)
- [ObjectMapper](https://github.com/Hearst-DD/ObjectMapper)
- [Swinject](https://github.com/Swinject/Swinject)
- [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON)
- [SlideMenuControllerSwift](https://github.com/dekatotoro/SlideMenuControllerSwift)
- [Alamofire](https://github.com/Alamofire/Alamofire)
- [RxSwift](https://github.com/ReactiveX/RxSwift)
- [R.swift](https://github.com/mac-cain13/R.swift)
- [Starscream](https://github.com/daltoniam/Starscream)
- [SugarRecord](https://github.com/pepibumur/SugarRecord)
- [SwiftGen](https://github.com/AliSoftware/SwiftGen)
- [SnapKit](https://github.com/SnapKit/SnapKit)

## Android

- [AndroidAnnotations](https://github.com/excilys/androidannotations)
- [RxBinding](https://github.com/JakeWharton/RxBinding)
- [Android-CleanArchitecture](https://github.com/android10/Android-CleanArchitecture)
- [EventBus](https://github.com/greenrobot/EventBus)
- [otto](https://github.com/square/otto)
- [RxLifecycle](https://github.com/trello/RxLifecycle)
- [Dagger 2](https://github.com/google/dagger)
- [RxJava](https://github.com/ReactiveX/RxJava)
- [nucleus](https://github.com/konmik/nucleus)
- [ImageBlurring](https://github.com/qiujuer/ImageBlurring)
- [MaterialDrawer](https://github.com/mikepenz/MaterialDrawer)
- [leakcanary](https://github.com/square/leakcanary)
- [mosby](https://github.com/sockeqwe/mosby)
- [RxAndroidAudio](https://github.com/Piasy/RxAndroidAudio)
- [RoboBinding](https://github.com/RoboBinding/RoboBinding)
- [agera](https://github.com/google/agera)
- [RxBus](https://github.com/AndroidKnife/RxBus)
- [Moxy](https://github.com/Arello-Mobile/Moxy)
- [realm](https://github.com/realm/realm-java)
- [facebook-android-sdk](https://github.com/facebook/facebook-android-sdk)
- [HockeySDK-Android](https://github.com/bitstadium/HockeySDK-Android)
- [merlin](https://github.com/novoda/merlin)
- [vk-android-sdk](https://github.com/VKCOM/vk-android-sdk)
- [retrofit](https://github.com/square/retrofit)
- [okhttp](https://github.com/square/okhttp)
- [gson](https://github.com/vparfonov/gson)
- [glide](https://github.com/bumptech/glide)
- [butterknife](https://github.com/JakeWharton/butterknife)

## C/C++
- [RxCpp](https://github.com/Reactive-Extensions/RxCpp)
- [Swift](https://github.com/apple/swift)

## RX
- [RxCpp](https://github.com/Reactive-Extensions/RxCpp)
- [RxJS](https://github.com/Reactive-Extensions/RxJS)
- [RxSwift](https://github.com/ReactiveX/RxSwift)
- [RxJava](https://github.com/ReactiveX/RxJava)
- [RxKotlin](https://github.com/ReactiveX/RxKotlin)

## Kotlin
- [RxKotlin](https://github.com/ReactiveX/RxKotlin)
- [Fuel](https://github.com/kittinunf/Fuel)
- [kotgo](https://github.com/nekocode/kotgo)
- [KAndroid](https://github.com/pawegio/KAndroid)
- [anko](https://github.com/Kotlin/anko)
- [injekt](https://github.com/kohesive/injekt)
- [kotlin-koans](https://github.com/Kotlin/kotlin-koans)
- [RxKotlin](https://github.com/ReactiveX/RxKotlin)
- [RxKotlin](https://github.com/ReactiveX/RxKotlin)

